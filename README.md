# Multiplayer-Snake

## Projektziel
Ziel ist die Erstellung eines Projektes, das geprägt ist durch die Verwendung von WebSocket-Kommunikation (socket.io). In diesem Experiment sollen nicht nur die Spieldaten in Echtzeit per WebSocket übermittelt, sondern auch sämtliche Nutzer-Interaktionen in der Website/Anwendung und die damit zusammenhängende asnychrone Kommunikation mit dem Server mittels WebSockets organisiert werden. Dadurch entsteht ein stark eventbasiertes Server-Konzept.  

"Testlast" dieses Projektes ist ein bewusst einfach gehaltenes Spiel (Snake), an dem die Prinzipien der WebSocket-Kommunikation exerziert werden können, ohne dass übermäßiger Nebenaufwand durch die Implementierung einer vielschichtigen Spielelogik entsteht. Um dennoch ein wenig Komplexität einzuführen, wurden verschiedene Spielmodi erstellt, zwischen denen der Nutzer wählen kann.

Der Aspekt der "verteilten Systeme" kommt über die Client-Server-Struktur hinzu. Jeder Client ist in der Lage bei jedem anderen Client vorhergesehene Änderungen vorzunehmen (z.B. Raum erstellen, Spielinteraktion), wodurch beim Nutzer insgesamt der Eindruck eines zusammenhängenden Systems entsteht. Die "emits" (von Socket.io ausgegebene Events, per WebSocket-Protokoll übermittelt) fungieren dabei nicht selten als quasi-RPCs, die wohldefinierte Funktionen im Server oder Client oder allen Clients auslösen.

## Verwendete Technologien
- JavaScript, HTML, CSS, P5js (Frontend)
- JavaScript/NodeJS (Backend)
	- Socket.io (Implementierung WebSocket)
	- Express (Server)


## Projektautoren

- Milena Sand
- Kevin Greiner
- Paul Genest
- Jan Reichelt

## Initialisierung

1. Repository Clonen: ```git clone https://gitlab.com/Alpel/multiplayer-snake.git```
2. Vom Wurzelordner aus in Server-Ordner wechseln: ```cd server```
3. Abhängigkeiten mit Node Package Manager installieren: ```npm install```
4. Server starten: ```node .```
5. Im Browser ```http://localhost:80``` aufrufen

## Live-Server

Eine laufende Version dieses Projektes kann über folgende URL erreicht werden: https://snakeworld.ddns.net/

## Lizenz

- General Public Licence (GPL)
