const BG_COLOR = '#231F20';
const FOOD_COLOR = '#E66916';

// socket.on('init', handleInit);
socket.on('gameState', handleGameState);
socket.on('leaveRoom', handleLeaveRoom);
socket.on('gameOver', handleGameOver);
socket.on('countDown', handleCountDown);

let canvas;
let gameState = null;
let scores = null;
let countdown = null;

/*
 *	SOCKET EVENT-LISTENER
 */

function handleGameState(serverGameState) {
	gameState = serverGameState;
}

function handleLeaveRoom() {
	gameState = null;
	scores = null;
	countdown = null;
}

function handleGameOver(scoreData) {
	gameState = null;
	scores = scoreData;
}

function handleCountDown(CountdownData) {
	countdown = CountdownData;
}

/*
 *	UTIL-FUNCTIONS
 */

function setup() {
	canvas = createCanvas(600, 600);
	canvas.parent('canvas-container');
	canvas.id('canvas');

	frameRate(60);
}

function keydown(e) {
	if ([37, 38, 39, 40].includes(e.keyCode)) {
		e.preventDefault();
	}
	socket.emit('keydown', e.keyCode);
}

/*
 *	FRONTENT/DRAWING FUNCTIONS
 */

function draw() {
	// 'reset' drawing screen
	fill(BG_COLOR);
	rect(0, 0, width, height);

	if (gameState) {
		// drawing of the gameState
		// parsing variables for the currentgamestate
		const food = gameState.food;
		const gridsize = gameState.gridsize;
		const size = width / gridsize;

		// draw food
		noStroke();
		fill(FOOD_COLOR);
		ellipse((food.x + 0.5) * size, (food.y + 0.5) * size, size, size);

		// draw snakes
		gameState.players.forEach(player => {
			paintPlayerP5(player, size, player.snakeColor);
		});

		// draw blocks - only in BattleRoyale gameMode:
		if ('blocks' in gameState) drawBlocks(gameState.blocks, size);

		// set the timer from the gamestate object
		timer.innerHTML = getTimerString(gameState.time);
	} else if (scores) {
		// if there is a scores object

		// constant for size of everything
		const highscoreSnakeScale = 15;

		for (let i = 0; i < scores.length; i++) {
			// get color of user and draw text
			fill(scores[i].color);
			textSize(30);
			text(scores[i].username + ' : ' + scores[i].score, 30, 60 * (i + 1.5));

			// draw snake according to length, reuse of paintPlayerP5() function
			const ySnake = 1 * (4 * i + 3);
			const player = { 'pos':{ 'x':2, 'y':ySnake }, 'snake':[{ 'x':2, 'y':ySnake }, { 'x':3, 'y':ySnake }] };
			const length = min(Math.floor(Math.sqrt(scores[i].score)), (width / highscoreSnakeScale) - 8);

			for (let j = 0; j < length; j++) {
				player.snake.push({ 'x': 4 + j, 'y': ySnake });
			}
			paintPlayerP5(player, highscoreSnakeScale, scores[i].color);
		}
	}
	if (countdown !== null) {
		// display of the countdown
		push();
		textAlign(CENTER, CENTER);
		fill(180);
		if (countdown > 0) {
			textSize(250);
			text(countdown, width / 2, height / 2);
		} else {
			textSize(150);
			text('START', width / 2, height / 2);
		}
		pop();
	}
}

/*
*	drawing the player(snake) on the canvas
*	@param: {Object} playerState - object containing all the specific player data
*	@param: {Integer} size - number of pixels each rect size should be
*	@param: {color} color - color this player should be drawn in
*/
function paintPlayerP5(playerState, size, color) {
	noStroke();
	// snake object in playerState contains all the positions
	const snake = playerState.snake;
	fill(color);
	for (const cell of snake) {
		rect(cell.x * size, cell.y * size, size, size);
	}

	// draw the eyes on the first rect (pos) of the player
	fill('white');
	ellipse(playerState.pos.x * size + 2, playerState.pos.y * size + 2, 10);
	ellipse(playerState.pos.x * size + size - 2, playerState.pos.y * size + 2, 10);
	fill('black');
	ellipse(playerState.pos.x * size + 2, playerState.pos.y * size + 2, 3);
	ellipse(playerState.pos.x * size + size - 2, playerState.pos.y * size + 2, 3);
}

/*
*	drawing the block in battle royale mode on canvas
*	@param: {Array} blocks - object containing all the blocks which should be drawn
*	@param: {Integer} size - number of pixels each rect size should be
*/
function drawBlocks(blocks, size) {
	blocks.forEach(block => {
		stroke('#777');
		strokeWeight(3);
		fill('#999');
		rect((block.x * size) + 2, (block.y * size) + 2, size - 4, size - 4);
	});
}

/*
*	converting the timer number into a string that can be displayed on top
*	@param: {Number} time - number of milliseconds passed since game start
*	@return: {String} complete number string, format mm:ss
*/
function getTimerString(time) {
	time = Math.floor(time / 1000);
	const seconds = time % 60;
	const minutes = Math.floor(time / 60);

	return `${minutes < 10 ? '0' + minutes : minutes}:${seconds < 10 ? '0' + seconds : seconds}`;
}
