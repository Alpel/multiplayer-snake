const { FRAME_RATE } = require('./constants');

/*
 *	The GameLoop class assembles all functions necessary to increment the gameState of a
 *	given game by one step. The central function this.run() automates this process and returns the
 *	current gameState, so that it can then be send to the client. The GameLoop_Base class is conceived as basis
 *	for inheritance and further modfication to create different game modes.
 *	@param: {Object GameState} gameState - the initial gameState object to increment on, with a full players list
 */
class GameLoop_Base {
	constructor(gameState) {
		this.gameState = gameState;
		this.FRAME_RATE = FRAME_RATE;
		this.generateFood();
	}

	/*
	 *	Central function of GameLoop-class -
	 *	increments the gameState, updating player-position, checks collisions etc.
	 *	@return {Object GameState} updated gameState object
	 */
	run() {
		if (!this.gameState) return;

		this.setTime();
		for (let i = this.gameState.players.length - 1; i >= 0; i--) {
			const player = this.gameState.players[i];
			this.updatePosition(player);
			this.checkCollisions(player);
			this.movePlayer(player);
		}

		return this.gameState;
	}

	/*
	 *	Updates the gameState.time property in ms since start of game
	 */
	setTime() {
		this.gameState.time = this.gameState.time + (1000 / FRAME_RATE);
	}

	/*
	 *	Updates the player position depending on its velocity values
	 *	@param: {Object Player} player - the player from gameState.players list
	 */
	updatePosition(player) {
		player.pos.x += player.vel.x;
		player.pos.y += player.vel.y;
	}

	/*
	 *	Checks if player position collides with other objects and lets him die if so
	 *	@param: {Object Player} player - the player from gameState.players list
	 */
	checkCollisions(player) {
		if (player.vel.x || player.vel.y) {
			// check walls
			if (player.pos.x < 0 ||
				player.pos.x > this.gameState.gridsize - 1 ||
				player.pos.y < 0 ||
				player.pos.y > this.gameState.gridsize - 1) {
				return this.playerDie(player);
			}

			// check own body
			for (const cell of player.snake) {
				if (cell.x === player.pos.x && cell.y === player.pos.y) {
					return this.playerDie(player);
				}
			}

			// check enemy snakes
			for (const enemy of this.gameState.players) {
				if (enemy.id !== player.id) {
					for (const cell of enemy.snake) {
						if (cell.x === player.pos.x && cell.y === player.pos.y) {
							// add dying players points to enemy
							enemy.points += Math.floor(player.points / 2);
							return this.playerDie(player);
						}
					}
				}
			}
		}
	}

	/*
	 *	The actual movement of the player, by adding one body segment at the end and removing one segment from start
	 *	Also generates new food if player moved over food and increments its points
	 *	@param: {Object Player} player - the player from gameState.players list
	 */
	movePlayer(player) {
		player.snake.push({ ...player.pos });

		// check for food eaten
		if (this.gameState.food.x === player.pos.x && this.gameState.food.y === player.pos.y) {
			player.points += 1;
			this.generateFood();
		} else {
			player.snake.shift();
		}
	}

	/*
	 *	Handles player death - remove player from gameState.players list and setting up points
	 *	@param: {Object Player} player - the player from gameState.players list
	 */
	playerDie(player) {
		const index = this.indexOf(player);

		if (index !== -1 && index !== null) {
			// Calculate points
			const points = this.evaluatePoints(player);

			this.gameState.players[index].points = points;
			this.gameState.scores.unshift({
				player:	player.id,
				color:	player.snakeColor,
				score:	points
			});

			// Remove player from player list
			this.gameState.players.splice(index, 1);
		}
	}

	/*
	 *	Calculation of player points (collected fruits * time)
	 *	@param: {Object Player} player - the player from gameState.players list
	 */
	evaluatePoints(player) {
		return Math.floor(player.points * this.gameState.time / 1000);
	}

	/*
	 *	Generates gameState.food position recursively and checks if this position is available
	 */
	generateFood() {
		const food = {
			x: Math.floor(Math.random() * this.gameState.gridsize),
			y: Math.floor(Math.random() * this.gameState.gridsize),
		};

		for (const player of this.gameState.players) {
			for (const cell of player.snake) {
				if (cell.x === food.x && cell.y === food.y) {
					return this.generateFood();
				}
			}
		}

		this.gameState.food = food;
	}

	/*
	 *	Searches player in gameState.players list and returns its index
	 *	@param: {Object Player} player - the player from gameState.players list
	 *	@return: {Integer || null} list index or null if player is not found
	 */
	indexOf(player) {
		if (player) return this.gameState.players.findIndex(p => p.id === player.id);
		return null;
	}
}

class GameLoop_NoCollision extends GameLoop_Base {
	constructor(gameState) {
		super(gameState);
	}

	checkCollisions(player) {
		if (player.vel.x || player.vel.y) {
			// check walls
			if (player.pos.x < 0 ||
				player.pos.x > this.gameState.gridsize - 1 ||
				player.pos.y < 0 ||
				player.pos.y > this.gameState.gridsize - 1) {
				return this.playerDie(player);
			}

			// check own body
			for (const cell of player.snake) {
				if (cell.x === player.pos.x && cell.y === player.pos.y) {
					return this.playerDie(player);
				}
			}
		}
	}
}

class GameLoop_NoWalls extends GameLoop_Base {
	constructor(gameState) {
		super(gameState);
	}

	updatePosition(player) {
		const gridsize = this.gameState.gridsize;

		if		(player.pos.x + player.vel.x > gridsize - 1) player.pos.x = 0;
		else if (player.pos.x + player.vel.x < 0) 			 player.pos.x = gridsize - 1;
		else player.pos.x += player.vel.x;

		if 		(player.pos.y + player.vel.y > gridsize - 1) player.pos.y = 0;
		else if (player.pos.y + player.vel.y < 0) 			 player.pos.y = gridsize - 1;
		else player.pos.y += player.vel.y;
	}

	checkCollisions(player) {
		if (player.vel.x || player.vel.y) {
			// check own body
			for (const cell of player.snake) {
				if (cell.x === player.pos.x && cell.y === player.pos.y) {
					return this.playerDie(player);
				}
			}

			// check enemy snakes
			for (const enemy of this.gameState.players) {
				if (enemy.id !== player.id) {
					for (const cell of enemy.snake) {
						if (cell.x === player.pos.x && cell.y === player.pos.y) {
							return this.playerDie(player);
						}
					}
				}
			}
		}
	}
}

class GameLoop_BattleRoyale extends GameLoop_Base {
	constructor(gameState) {
		super(gameState);
		this.gameState['blocks'] = [];
		this.blocks = this.buildBlocks();
	}

	/*
	 *	Prepares blocks-pattern for this gameMode, which is later accessed by this.addBlock()
	 */
	buildBlocks() {
		let side = 't'; // 't', 'r', 'b', 'l' - top, right, bottom, left
		let max = this.gameState.gridsize - 1;
		let min = 0;
		const blocks = [];

		while (min <= max) {
			if (side === 't') {
				for (let i = min + 1; i < max; i++) {
					blocks.push({ x: i, y: min });
				}
				side = 'r';
			}
			if (side === 'r') {
				for (let i = min + 1; i < max; i++) {
					blocks.push({ x: max, y: i });
				}
				side = 'b';
			}
			if (side === 'b') {
				for (let i = max - 1; i > min; i--) {
					blocks.push({ x: i, y: max });
				}
				max -= 1;
				side = 'l';
			}
			if (side === 'l') {
				for (let i = max; i > min; i--) {
					blocks.push({ x: min, y: i });
				}
				min += 1;
				side = 't';
			}
		}
		return blocks;
	}

	run() {
		if (!this.gameState) return;

		this.setTime();
		this.addBlock();
		for (let i = this.gameState.players.length - 1; i >= 0; i--) {
			const player = this.gameState.players[i];
			this.updatePosition(player);
			this.checkCollisions(player);
			this.movePlayer(player);
		}

		return this.gameState;
	}

	/*
	 *	Adds blocks as obstacles to the gameState.blocks array in a given interval
	 */
	addBlock() {
		// Add ten blocks per second
		const time = Math.floor(this.gameState.time / 100);

		if (this.blocks[time]) this.gameState.blocks.push(this.blocks[time]);

		// Generate new food if one is covered by the new block
		if (this.blocks[time].x === this.gameState.food.x &&
			this.blocks[time].y === this.gameState.food.y) {
			this.generateFood();
		}
	}

	checkCollisions(player) {
		if (player.vel.x || player.vel.y) {
			// check walls
			if (player.pos.x < 0 ||
				player.pos.x > this.gameState.gridsize - 1 ||
				player.pos.y < 0 ||
				player.pos.y > this.gameState.gridsize - 1) {
				return this.playerDie(player);
			}

			// check blocks
			for (const block of this.gameState.blocks) {
				if (block.x === player.pos.x && block.y === player.pos.y) {
					return this.playerDie(player);
				}
			}


			// check own body
			for (const cell of player.snake) {
				if (cell.x === player.pos.x && cell.y === player.pos.y) {
					return this.playerDie(player);
				}
			}

			// check enemy snakes
			for (const enemy of this.gameState.players) {
				if (enemy.id !== player.id) {
					for (const cell of enemy.snake) {
						if (cell.x === player.pos.x && cell.y === player.pos.y) {
							return this.playerDie(player);
						}
					}
				}
			}
		}
	}

	generateFood() {
		const food = {
			x: Math.floor(Math.random() * this.gameState.gridsize),
			y: Math.floor(Math.random() * this.gameState.gridsize),
		};


		for (const player of this.gameState.players) {
			for (const cell of player.snake) {
				if (cell.x === food.x && cell.y === food.y) {
					return this.generateFood();
				}
			}
		}

		if (this.gameState.blocks) {
			for (const block of this.gameState.blocks) {
				if (block.x === food.x && block.y === food.y) {
					return this.generateFood();
				}
			}
		}

		this.gameState.food = food;
	}
}

module.exports = {
	GameLoop_Base:		   GameLoop_Base,
	GameLoop_NoCollision:  GameLoop_NoCollision,
	GameLoop_NoWalls:	   GameLoop_NoWalls,
	GameLoop_BattleRoyale: GameLoop_BattleRoyale
};
