const { GRID_SIZE, COUNTDOWN_TIMER } = require('./constants');

module.exports = {
	createGameState,
	getUpdatedVelocity,
	addPlayerToGameState
};

/*
 *	SETTING UP GAMESTATE
 */

 /*
 *  generates the gamestate to start the game based on the number of players
 *	@param: {Array of players} players - the player objects which should be included in the gamestate
 *  @return: {Object} gameStateTemplate - gameState that can be started with gameLoop
 */
function createGameState(players) {
	const gameStateTemplate = {
		players:[],
		scores:[],
		food: {},
		gridsize: GRID_SIZE,
		time: 0,
		countdown: COUNTDOWN_TIMER
	};

	if (Array.isArray(players)) {
		for (const player of players) {
			addPlayerToGameState(gameStateTemplate, player);
		}
	} else {
		addPlayerToGameState(gameStateTemplate, players);
	}
	return gameStateTemplate;
}

/*
*  adds a player to the gamestate
*	 @param: {Object} gameState - the current gamestate which the player should be added to
*	 @param: {Object} player - the data of the player that should be added
*  @return: {Object} gameState - gamestate with added player
*/
function addPlayerToGameState(gameState, player) {
	let tempPlayer = generatePlayerTemplate();

	while (checkSnakeStart(gameState, tempPlayer) === false) {
		tempPlayer = generatePlayerTemplate();
	}

	tempPlayer.id = player.id;
	tempPlayer.snakeColor = player.snakeColor;
	tempPlayer.points = 1;
	tempPlayer.velUpdate = true;

	gameState.players.push(tempPlayer);
	return gameState;
}

/*
*  generating a player template that starts at a random position
*  @return {Object} - random player
*/
function generatePlayerTemplate() {
	const playerTemplate = {
		pos:{ x:3, y:10, },
		vel:{ x:1, y:0, },
		snake:[{ x:1, y:10 }, { x:2, y:10 }, { x:3, y:10 }]
	};

	// snakes always start at a random side facing into the middle
	switch (getRandomInt(0, 4)) {
	case 0: {
		const y = randomGridSidePoint();
		playerTemplate.pos.y = y;
		playerTemplate.snake.forEach(part => part.y = y);
		break;
	}
	case 1: {
		const x = randomGridSidePoint();
		playerTemplate.pos.x = x;
		playerTemplate.pos.y = 3;
		playerTemplate.vel.x = 0;
		playerTemplate.vel.y = 1;
		playerTemplate.snake.forEach((part, i) => {
			part.x = x;
			part.y = i + 1;
		});
		break;
	}
	case 2: {
		const y = randomGridSidePoint();
		playerTemplate.pos.y = y;
		playerTemplate.pos.x = GRID_SIZE - 4;
		playerTemplate.vel.x = -1;
		playerTemplate.snake.forEach((part, i) => {
			part.y = y;
			part.x = GRID_SIZE - (i + 2);
		});
		break;
	}
	case 3: {
		const x = randomGridSidePoint();
		playerTemplate.pos.x = x;
		playerTemplate.pos.y = GRID_SIZE - 4;
		playerTemplate.vel.x = 0;
		playerTemplate.vel.y = -1;
		playerTemplate.snake.forEach((part, i) => {
			part.x = x;
			part.y = GRID_SIZE - (i + 2);
		});
		break;
	}
	}
	return playerTemplate;
}

/*
*  check the starting position of a player
*  @param {Object} gameStateTemplate - current gamestate
*  @param {Object} player - player to check
*  @return {Boolean} - is the player viable for this gamestate or not?
*/

function checkSnakeStart(gameStateTemplate, player) {
	let viable = true;

	if (gameStateTemplate.players.length === 0) {
		return viable;
	} else {
		gameStateTemplate.players.forEach(snake => {
			// also cancels players out that are starting on diagonally oppposite positions
			if (Math.abs(snake.pos.x - player.pos.x) <= 1 && Math.abs(snake.pos.y - player.pos.y) <= 1) {
				viable = false;
			}
		});
		for (const cell of player.snake) {
			if (cell.x === gameStateTemplate.food.x && cell.y === gameStateTemplate.food.y) {
				viable = false;
			}
		}
		return viable;
	}
}

/*
 *	UTIL FUNCTIONS
 */

 /*
 *  generates a random int beteween to numbers (not including the max value)
 *  @param {Number} min - minimum int
 *  @param {Number} max - maximum int
 */
function getRandomInt(min, max) {
	// max is not inclusive
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

/*
*  generates a value for the snakes to start from on the side, excluding the top and bottom quart
*/
function randomGridSidePoint() {
	const quart = GRID_SIZE / 4;
	return getRandomInt(quart, GRID_SIZE - quart); // starting in the middle half
}

/*
*  generates the new vel object based on the keyCode
*  @param {Number} keyCode - keyCode parsed form client input
*/
function getUpdatedVelocity(keyCode) {
	switch (keyCode) {
	case 37: // left
	case 65: {
		return { x: -1, y: 0 };
	}
	case 38: // down
	case 87: {
		return { x: 0, y: -1 };
	}
	case 39: // right
	case 68: {
		return { x: 1, y: 0 };
	}
	case 40: // up
	case 83: {
		return { x: 0, y: 1 };
	}
	default: return null;
	}
}
