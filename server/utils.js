const { COLORS } = require('./constants');
const fs = require('fs').promises;
const path = require('path');
module.exports = {
	createFlatClientRooms,
	generateRoomId,
	assignRandomColor,
	generateFlatPlayer,
	readHighscores,
	writeHighscores,
};

/*
*	creates a flat clientRooms Object containing all rooms without their password (for sending them to the clients)
*	@param: {Object} clientRooms - Object containing all rooms
* @return {Object} flatRooms - Object containing all rooms with the desired (less) data
*/
function createFlatClientRooms(clientRooms) {
	const flatRooms = {};

	for (const [key, value] of Object.entries(clientRooms)) {
		flatRooms[key] = {
			name: value.name,
			maxPlayers: value.maxPlayers,
			roomId: value.roomId,
			isPrivate: value.isPrivate,
			gameMode: value.gameMode,
			players: value.players,
			gameStarted: value.gameStarted
		};
	}
	return flatRooms;
}

/*
*	generates a random ID for a new room - checks if the ID is avaiable
*	@param: {Object} clientRooms - Object containing all rooms (to check for duplicate IDs)
*	@return: {String} result - the id for the new room
*/
function generateRoomId(clientRooms) {
	let result = '';
	const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	const charactersLength = characters.length;
	for (let i = 0; i < 15; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	if (result in clientRooms) return generateRoomId();
	return result;
}

/*
*	assings a player a random color from the COLORS array upon joining a room - checks if this color is avaiable
*	@param: {Object} room - the corresponding (to check for duplicate colors)
*	@return: {String} color - the color for the player
*/
function assignRandomColor(room) {
	const color = COLORS[Math.floor(Math.random() * COLORS.length)];

	// check if color is unavaiable
	for (const player of room.players) {
		if (color === player.snakeColor) {
			return assignRandomColor(room);
		}
	}
	return color;
}

/*
*	creates a flat player Object to add to the gameState
*	@param: {Object} player - the players object that gets flattened
*	@return: {Object} tempFlatPlayer - the player object with less info
*/
function generateFlatPlayer(player) {
	const tempFlatPlayer = {
		id: player.userid,
		snakeColor: player.snakeColor
	};
	return tempFlatPlayer;
}

/*
*	reads the HIGHSCORES array from the highscores.json - resets the highscores if file wasn't found
*	@return: {Array} HIGHSCORES - Array of the global highscores (is full of 0s if no file found)
*/
async function readHighscores() {
	try {
		const data = await fs.readFile(path.join(__dirname, 'highscores.json'), 'utf8');
		return JSON.parse(data);
	} catch (e) {
		console.log(`Error Reading Highscores: ${e}\nResetting Highscores!`);
		return [{ 'mode': 'SP', 'score': 0, 'username': '---' }, { 'mode': 'MP', 'score': 0, 'username': '---' }, { 'mode': 'NC', 'score': 0, 'username': '---' }];
	}
}

/*
*	updates the highscores.json file and send the new array to all clients
*	@param: {Array} HIGHSCORES - the updated highscores array to write into the file
*/
function writeHighscores(HIGHSCORES) {
	fs.writeFile(path.join(__dirname, 'highscores.json'), JSON.stringify(HIGHSCORES), err => {
		if (err) return console.log(`Error Writing Highscores: ${err}`);
	});
}
