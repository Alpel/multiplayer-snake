// game ticks per second
const FRAME_RATE = 15;
// number of blocks in both directions for snake to move
const GRID_SIZE = 40;
// how many seconds will be counted down before game starts
const COUNTDOWN_TIMER = 3;
// possible colors for players to choose from for their snakes
const COLORS = [
	'#ff0000',  // red
	'#00ff00',  // green
	'#0000ff',  // blue
	'#ffff00',  // yellow
	'#ff00ff',  // fuchsia
	'#00ffff',  // aqua
	'#8000ff',  // violet
	'#007f00',  // darkgreen
	'#c284ff',  // lavender
	'#c2c2c2'   // grey
];

module.exports = {
	FRAME_RATE,
	GRID_SIZE,
	COLORS,
	COUNTDOWN_TIMER
};
