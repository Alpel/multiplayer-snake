const {
	createGameState,
	getUpdatedVelocity,
	addPlayerToGameState } = require('./game');
const {
	createFlatClientRooms,
	generateRoomId,
	assignRandomColor,
	generateFlatPlayer,
	readHighscores,
	writeHighscores } = require('./utils');
const { FRAME_RATE, COLORS } = require('./constants');
const GameLoop = require('./gameLoop');

const fs = require('fs');
const https = require('https');
const express = require('express');
const ioServer = require('socket.io');

const app = express();
const httpsapp = express();
const PORT = 80;
const HTTPSPORT = 443;

const CLIENTROOMS = {};
const USERS = {};
let HIGHSCORES = [];

const server = app.listen(PORT, '0.0.0.0', () => {
	console.log(`Listening at http://localhost:${PORT}`);
});

const io = new ioServer();
io.attach(server);

let httpsServer;
// Certificate
try {
	const privateKey = fs.readFileSync('/etc/letsencrypt/live/snakeworld.ddns.net/privkey.pem', 'utf8');
	const certificate = fs.readFileSync('/etc/letsencrypt/live/snakeworld.ddns.net/cert.pem', 'utf8');
	const ca = fs.readFileSync('/etc/letsencrypt/live/snakeworld.ddns.net/chain.pem', 'utf8');

	const options = {
		key: privateKey,
		cert: certificate,
		ca: ca
	};

	httpsServer = https.createServer(options, httpsapp).listen(HTTPSPORT, '0.0.0.0', function() {
		console.log('HTTPS enabled: Express server listening on port: ' + HTTPSPORT);
	});
	io.attach(httpsServer);
} catch (e) {
	console.log('No HTTPS enabled: Express server listening on port: ' + PORT);
} finally {
	httpsForwarding(httpsServer);
}

/*
*	Setup of the express apps to ensure working server and possible https redirect
*	@param: {Boolean} listening - is the httpsServer working?
*/
function httpsForwarding(listening) {
	if (listening) {
		// https forward if httpsServer is running
		// Middleware
		app.use((req, res) => {
			res.redirect('https://' + req.headers.host + req.url);
		});
		console.log("http:// to https:// redirect activated");
		// server normal website on https
		httpsapp.use(express.static(__dirname + '/../frontend'));

		// JSON-Data Routes on https
		httpsapp.get('/userdata', (req, res) => {
			res.json(USERS);
		});
		httpsapp.get('/highscoresdata', (req, res) => {
			res.json(HIGHSCORES);
		});
		httpsapp.get('/roomdata', (req, res) => {
			res.json(createFlatClientRooms(CLIENTROOMS));
		});
	} else {
		// if no httpsServer serve everything on http
		// Middleware
		app.use(express.static(__dirname + '/../frontend'));
		console.log("entire app runs on http://");
		// JSON-Data Routes
		app.get('/userdata', (req, res) => {
			res.json(USERS);
		});
		app.get('/highscoresdata', (req, res) => {
			res.json(HIGHSCORES);
		});
		app.get('/roomdata', (req, res) => {
			res.json(createFlatClientRooms(CLIENTROOMS));
		});
	}
}

// getting Highscores data after read in complete
readHighscores().then(data => {
	HIGHSCORES = data;
});

// Test data
// USERS[1] = { userid: 1, username: 'Paul', inRoom: null, avatar: 'https://avatars.dicebear.com/api/male/paul.svg', scores : [{ 'mode': 'MP', 'score': 71 }, { 'mode': 'SP', 'score': 5 }]};
// USERS[2] = { userid: 2, username: 'Jan', inRoom: null, avatar: 'https://avatars.dicebear.com/api/male/jan.svg',scores : [] };
// USERS[3] = { userid: 3, username: 'Milena', inRoom: null, avatar: 'https://avatars.dicebear.com/api/female/milena.svg', scores : [{ 'mode': 'SP', 'score': 10 }] };
// USERS[4] = { userid: 4, username: 'Kevin', inRoom: null, avatar: 'https://avatars.dicebear.com/api/male/kevin.svg',scores : [{ 'mode': 'SP', 'score': 20 }] };
CLIENTROOMS[1] = { name: 'RainbowParty', maxPlayers: 4, roomId: 1, password: null, isPrivate: false, gameMode: 'standard', players: [], gameStarted: false };
CLIENTROOMS[2] = { name: 'SnakePros', maxPlayers: 6, roomId: 2, password: 1234, isPrivate: true, gameMode: 'NC', players: [], gameStarted: false };

// #############
// EMIT HANDLING
// #############

io.on('connection', client => {
	client.on('newUser', handleNewUser);
	client.on('newRoom', handleNewRoom);
	client.on('disconnecting', handleDisconnect);
	client.on('joinRoom', handleJoinRoom);
	client.on('leaveRoom', handleLeaveRoom);
	client.on('checkPassword', handleCheckPassword);
	client.on('sendInvite', handleInvitePlayer);
	client.on('reqRoomPlayerColors', handleColorRequest);
	client.on('changeSnakeColor', handleChangeSnakeColor);
	client.on('isReady', handleIsReady);
	client.on('keydown', handleKeydown);

	/*
 	*	Submit-Handler for a new Player - sets up his user Object and sends him all necessary data
 	*	@param: {Object} newUser - Object containing the newly connected Users data
 	*/
	function handleNewUser(newUser) {
		USERS[newUser.userid] = newUser;
		newUser.scores = [];
		// newUser.scores = [{ 'mode': 'SP', 'score': 0 }, { 'mode': 'MP', 'score': 0 }, { 'mode': 'NC', 'score': 0 }];
		newUser.roomScores = [];

		// setStatusScore(newUser.userid);

		io.emit('updateUserList', USERS);
		client.emit('updateRoomList', createFlatClientRooms(CLIENTROOMS));
		client.emit('updateHighscores', HIGHSCORES);
	}

	/*
 	*	Submit-Handler for a new Room - validates the input data; creates the room if valid
 	*	@param: {Object} newRoom - Object containing the rooms data
 	*/
	function handleNewRoom(newRoom) {
		newRoom.roomId = generateRoomId(CLIENTROOMS);

		// validate input
		if (newRoom.name === '' || newRoom.maxPlayers === '' || newRoom.gameMode === '') {
			client.emit('refuseNewRoom', 'empty');
			return;
		}
		if (newRoom.maxPlayers < 1 || newRoom.maxPlayers > 6) {
			client.emit('refuseNewRoom', 'maxPlayers');
			return;
		}
		let nameTaken = false;
		for (const value of Object.values(CLIENTROOMS)) {
			if (newRoom.name.toLowerCase() === value.name.toLowerCase()) nameTaken = true;
		}
		if (nameTaken) {
			client.emit('refuseNewRoom', 'nameTaken');
			return;
		}

		CLIENTROOMS[newRoom.roomId] = newRoom;
		handleJoinRoom(newRoom.roomId);
		io.emit('updateRoomList', createFlatClientRooms(CLIENTROOMS));
	}

	/*
 	*	Submit-Handler for disconnection - called, when a client closes the connection; deletes the users data
 	*/
	function handleDisconnect() {
		// avoid old session bug
		if (USERS[client.id]) {
			// check if the player was in a room => remove him there
			handleLeaveRoom();

			delete USERS[client.id];
			io.emit('updateUserList', USERS);
		}
	}

	/*
 	*	Submit-Handler for a joining a room - puts the client in this room if he's allowed to join
 	*	@param: {String} roomId - ID of the room the client wants to join
 	*/
	function handleJoinRoom(roomId) {
		const user = USERS[client.id];

		// check if the user is in this room already
		if (user.inRoom === roomId) return;

		const newRoom = CLIENTROOMS[roomId];

		// check if the room is full
		if (newRoom.players.length >= newRoom.maxPlayers) {
			client.emit('roomAlreadyFull');
			return;
		}

		// check if the user is in another room and leave it if so
		handleLeaveRoom();

		user.inRoom = roomId;
		user.snakeColor = assignRandomColor(newRoom);

		newRoom.players.push(user);
		client.join(roomId);

		// send the player the first gameState, if other users are ready
		if (newRoom.gameState != null) {
			client.emit('gameState', newRoom.gameState);
		}

		const flatRooms = createFlatClientRooms(CLIENTROOMS);

		client.emit('joinRoom', flatRooms[roomId]);
		io.emit('updateRoomList', flatRooms);
		io.in(roomId).emit('updateRoomPlayerList', flatRooms[roomId].players);
	}

	/*
 	*	Submit-Handler for when a player leaves a room - removes the client from the rooms playerlist and gameState
 	*/
	function handleLeaveRoom() {
		const user = USERS[client.id];
		const roomId = user.inRoom;

		if (!roomId) return;	// Client isn't in any room
		const room = CLIENTROOMS[roomId];

		user.inRoom = null;
		user.isReady = false;
		user.roomScores = [];

		room.players = room.players.filter(player => player.userid !== client.id);

		// remove the player from the gamestate if the game was already created
		if (room.gameState != null) {
			room.gameState.players = room.gameState.players.filter(player => player.id !== client.id);
			io.in(roomId).emit('gameState', room.gameState);
		}
		client.leave(roomId);

		// delete room when last Player leaves
		if (room.players.length === 0) {
			delete CLIENTROOMS[roomId];
		} else {
			io.in(roomId).emit('updateRoomPlayerList', room.players);
			CheckEveryoneReady(room);
		}
		io.emit('updateRoomList', createFlatClientRooms(CLIENTROOMS));
		client.emit('leaveRoom');
	}

	/*
 	*	Submit-Handler for checking a rooms password - lets the user join the room, if he submits the correct password
 	*	@param: {String} roomId - ID of the room the client wants to join
	*	@param: {String} password - password submitted by the client
 	*/
	function handleCheckPassword(roomId, password) {
		const room = CLIENTROOMS[roomId];

		if (room) {
			if (room.password.toString() === password) {
				return client.emit('passwordValid', true, room.roomId);
			}
		}

		client.emit('passwordValid', false, roomId);
	}

	/*
 	*	Submit-Handler for inviting a player - if the invitation is valid, sends this player an invitation to the room the client is in
 	*	@param: {String} playerId - ID of the player the clients wants to invite
 	*/
	function handleInvitePlayer(playerId) {
		const inviter = USERS[client.id];
		const inviterRoom = CLIENTROOMS[inviter.inRoom];
		const player = USERS[playerId];
		const inRoom = player.inRoom;
		let failReason;

		// check if invite is valid
		if (inviterRoom == null) {
			failReason = 'noInviterRoom';
		} else if (inRoom != null) {
			failReason = 'alreadyInRoom';
		} else if (inviterRoom.players.length >= inviterRoom.maxPlayers) {
			failReason = 'roomFull';
		}

		// emit invite invalid message
		if (failReason) client.emit('inviteFailed', failReason);
		// invite is valid => emit invite to player
		else io.to(playerId).emit('playerInvited', inviterRoom.roomId, inviter.username);
	}

	/*
 	*	Submit-Handler for a color request - sends the client the remaining avaiable snake colors in this room
 	*/
	function handleColorRequest() {
		const room = CLIENTROOMS[USERS[client.id].inRoom];
		const playerColors = room.players.map(p => p.snakeColor);
		const colorList = COLORS.filter(c => !playerColors.includes(c));

		client.emit('remainingRoomColors', client.id, colorList);
	}

	/*
 	*	Submit-Handler for changing the snake color - changes the color of the clients snake in the room he is in
 	*	@param: {String} color - the color the user wants to switch to
 	*/
	function handleChangeSnakeColor(color) {
		USERS[client.id].snakeColor = color;
		const room = CLIENTROOMS[USERS[client.id].inRoom];

		io.in(room.roomId).emit('updateRoomPlayerList', room.players);
	}

	/*
 	*	Submit-Handler for the client signifying he's ready to play - generates the rooms gamestate or adds this player to it
 	*/
	function handleIsReady() {
		const user = USERS[client.id];
		const room = CLIENTROOMS[user.inRoom];

		if (room.gameStarted) return;

		// set player ready and update UI
		user.isReady = true;
		io.in(room.roomId).emit('updateRoomPlayerList', room.players);

		// generate gameState containing player or add player to existing gameState
		if (room.gameState == null) {
			room.gameState = createGameState(generateFlatPlayer(user));
		} else {
			addPlayerToGameState(room.gameState, generateFlatPlayer(user));
		}

		// send game-start preview
		io.in(room.roomId).emit('gameState', room.gameState);
		CheckEveryoneReady(room);
	}

	/*
 	*	Submit-Handler for submitting a keyDown event - changes this players velocity in the game
 	*	@param: {String} keyCode - the Code of the key the client pressed
 	*/
	function handleKeydown(keyCode) {
		if (!USERS[client.id]) return;

		const room = CLIENTROOMS[USERS[client.id].inRoom];

		if (typeof room === 'undefined') return;
		if (!room.gameStarted) return;

		try {
			keyCode = parseInt(keyCode);	// handle foreign keyboard input
		} catch (e) {
			console.error(e);
			return;
		}

		const vel = getUpdatedVelocity(keyCode);
		const player = room.gameState.players.find(p => p.id === client.id);

		if (vel && player) {
			// If no 180°-turn update velocity
			if (player.vel.x * vel.x + player.vel.y * vel.y !== -1 &&
				(player.vel.x !== vel.x && player.vel.y !== vel.y)) {
				if (player.velUpdate) {
					player.vel = vel;
					player.velUpdate = false;
				}
			}
		}
	}
}); // END of io.on('connection', client => {});

// ################
// HELPER FUNCTIONS
// ################

/*
*	checks if every player in this room is ready to play - if so, starts the countdown
*	@param: {Object} room - the corresponding room to check for
*/
function CheckEveryoneReady(room) {
	if (!room) return;
	if (room.players.every(player => player.isReady) && room.gameStarted === false) {
		room.gameStarted = true;

		startCountdown(room);

		room.players.forEach(player => player.isReady = false); // in this spot for canceling ready-animation in UI
		io.in(room.roomId).emit('gameStart');
		io.emit('updateRoomList', createFlatClientRooms(CLIENTROOMS));
	}
}

/*
*	starts the countdown in this room - when its over, start the gameInterval
*	@param: {Object} room - the corresponding room
*/
function startCountdown(room) {
	// update UI
	io.in(room.roomId).emit('gameState', room.gameState);
	io.in(room.roomId).emit('countDown', room.gameState.countdown);

	// countdown the timer
	const intervalId = setInterval(() => {
		if (room.gameState.countdown > 0) {
			room.gameState.countdown--;
			io.in(room.roomId).emit('countDown', room.gameState.countdown);
		} else {
			clearInterval(intervalId);
			io.in(room.roomId).emit('countDown', null);
			// start the game
			startGameInterval(room);
		}
	}, 1000);
}

/*
*	starts the game with an Interval of FRAME_RATE
*	chooses the gameLoop function corresponding to the rooms gameMode
*	on gameover evaluate the players points and update the highscores
*	@param: {Object} room - the corresponding room to start the game in
*/
function startGameInterval(room) {
	// Multiplayer vs Singleplayer
	room.gameState.isMP = (room.players.length > 1) ? true : false;

	let gameLoop;
	switch (room.gameMode) {
	case 'standard':
		gameLoop = new GameLoop.GameLoop_Base(room.gameState);
		break;
	case 'NC':
		gameLoop = new GameLoop.GameLoop_NoCollision(room.gameState);
		break;
	case 'INF':
		gameLoop = new GameLoop.GameLoop_NoWalls(room.gameState);
		break;
	case 'BR':
		gameLoop = new GameLoop.GameLoop_BattleRoyale(room.gameState);
		break;
	default:
		gameLoop = new GameLoop.GameLoop_Base(room.gameState);
		break;
	}

	const intervalId = setInterval(() => {
		if (!room.gameState.isMP && room.gameState.players.length === 1 ||
			room.gameState.isMP && room.gameState.players.length > 1) {

			room.gameState = gameLoop.run();	// Game magic happens here
			// reset boolean that player can change velocity once per gametick
			room.gameState.players.forEach(player => player.velUpdate = true);

			io.in(room.roomId).emit('gameState', room.gameState);
		} else {
			clearInterval(intervalId);

			// Evaluate points if only one is left in MP - else, in case of draw, GameLoop.playerDie() evaluated points already
			if (room.gameState.isMP && room.gameState.players[0]) {
				const winner = room.gameState.players[0];
				const winnerPoints = gameLoop.evaluatePoints(winner);

				room.gameState.players[0].points = winnerPoints;
				room.gameState.scores.unshift({
					player: winner.id,
					color: winner.snakeColor,
					score: winnerPoints
				});
			}

			// send scores to users to display in canvas
			room.gameState.scores.forEach(score => {
				score.username = room.players.find(p => p.userid === score.player).username;
			});
			io.in(room.roomId).emit('gameOver', room.gameState.scores);

			// set global highscores in user list
			let tempScore;
			room.gameState.scores.forEach(score => {
				tempScore = generateScore(room.gameMode, room.gameState.isMP, score);
				addScore(USERS[score.player].scores, tempScore);
				addScore(USERS[score.player].roomScores, tempScore);
				addScore(HIGHSCORES, tempScore, score.player);
			});

			// reset the roomspecific game variables
			room.gameStarted = false;
			room.gameState = null;

			io.emit('updateUserList', USERS);
			io.in(room.roomId).emit('updateRoomPlayerList', room.players);
			io.emit('updateRoomList', createFlatClientRooms(CLIENTROOMS)); // remove game-started state from room listitem in RoomList (UI)
		}
	}, 1000 / FRAME_RATE);
}

// #####################
// SETTING UP HIGHSCORES
// #####################

/*
* generates a score object from the given parameters that includes the score and gamemode
*	@param: {String} gameMode - the gamemode from the gameState
*	@param: {Boolean} isMP - if the gamestate is Multiplayer
*	@param: {Number} score - the calculated score
*	@return: {Object} tempScore - the score object
*/
function generateScore(gameMode, isMP, score) {
	const mode = gameMode !== 'standard' ? gameMode : isMP ? 'MP' : 'SP';
	const tempScore = {
		mode: mode,
		score: score.score
	};
	return tempScore;
}

/*
* adds a score to a given 'scoreboard' -> array of multiple score objects
*	@param: {Array of scores} scoreBoard - the target Array of multiple score objects
*	@param: {Object} score - score object -> see generateScore()
*	@param: {String} id - the id of the player, only used for global Highscores object, sets avatar & username
*/
function addScore(scoreBoard, score, id) {
	if (typeof scoreBoard.find(s => s.mode === score.mode) === 'undefined') {
		if (id) {
			score.username = USERS[id].username;
			score.avatar = USERS[id].avatar;
		}
		scoreBoard.push(score);
		scoreBoard.sort((a, b) => (a.score > b.score) ? -1 : ((b.score > a.score) ? 1 : 0));
	} else {
		const i = scoreBoard.find(s => s.mode === score.mode);
		if (i.score < score.score) {
			i.score = score.score;
			if (id) {
				i.username = USERS[id].username;
				i.avatar = USERS[id].avatar;
			}
			scoreBoard.sort((a, b) => (a.score > b.score) ? -1 : ((b.score > a.score) ? 1 : 0));
		}
	}

	if (id) writeHighscores(HIGHSCORES);
	io.emit('updateHighscores', HIGHSCORES);
}
